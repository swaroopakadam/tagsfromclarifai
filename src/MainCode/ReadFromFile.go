package main

import (
	"bufio"
	"os"
	"github.com/clarifai/clarifai-go"
	"encoding/json"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	owm "github.com/briandowns/openweathermap"
	"fmt"

)
func check(e error) {
	if e != nil {
		panic(e.Error())

	}
}

func PrepareConnection() *mgo.Collection {

	db :="instadb"
	coll :="Images"
	url := "mongodb://admin:admin@ds019658.mlab.com:19658/instadb"
	session, err := mgo.Dial(url)
	check(err)
	c:=session.DB(db).C(coll)
	return c

}
func UpdateTagsInDB(entry map[string]interface{},tagData *clarifai.TagResp){
	connection:=PrepareConnection()

	count, err := connection.Find(bson.M{"id": entry["id"].(string)}).Limit(1).Count()
	//var clarifaiResponse map[string]interface{}
	classes :=  bson.M{"$set": bson.M{"additionalTags.Classes": tagData.Results[0].Result.Tag.Classes}}
	probs :=  bson.M{"$set": bson.M{"additionalTags.Probs": tagData.Results[0].Result.Tag.Probs}}
	catIds := bson.M{"$set": bson.M{"additionalTags.CatIds": tagData.Results[0].Result.Tag.CatIDs}}
	if err != nil {
		check(err)
	}
	if count > 0 {
		if err != nil {
			panic(err)
		}
		connection.Update(bson.M{"id": entry["id"]},classes)
		connection.Update(bson.M{"id": entry["id"]},probs)
		connection.Update(bson.M{"id": entry["id"]},catIds)

	}

}

func UpdateWeatherDataInDB(entry map[string]interface{},weatherData *owm.CurrentWeatherData){
	connection:=PrepareConnection()

	count, err := connection.Find(bson.M{"id": entry["id"].(string)}).Limit(1).Count()

	Main :=  bson.M{"$set": bson.M{"weatherData.Weather.Main": weatherData.Weather[0].Main}}
	Description :=  bson.M{"$set": bson.M{"weatherData.Weather.Description": weatherData.Weather[0].Description}}

	Temp :=  bson.M{"$set": bson.M{"weatherData.Main.Temp": weatherData.Main.Temp}}
	TempMin := bson.M{"$set": bson.M{"weatherData.Main.TempMin": weatherData.Main.TempMin}}
	TempMax := bson.M{"$set": bson.M{"weatherData.Main.TempMax": weatherData.Main.TempMax}}
	Pressure := bson.M{"$set": bson.M{"weatherData.Main.Pressure": weatherData.Main.Pressure}}
	SeaLevel := bson.M{"$set": bson.M{"weatherData.Main.SeaLevel": weatherData.Main.SeaLevel}}
	GrndLevel := bson.M{"$set": bson.M{"weatherData.Main.GrndLevel": weatherData.Main.GrndLevel}}
	Humidity := bson.M{"$set": bson.M{"weatherData.Main.Humidity": weatherData.Main.Humidity}}

	Speed := bson.M{"$set": bson.M{"weatherData.Wind.Speed": weatherData.Wind.Speed}}
	Deg := bson.M{"$set": bson.M{"weatherData.Wind.Deg": weatherData.Wind.Deg}}

	Clouds := bson.M{"$set": bson.M{"weatherData.Clouds.All": weatherData.Clouds.All}}

	if err != nil {
		check(err)
	}
	if count > 0 {
		if err != nil {
			panic(err)
		}
		connection.Update(bson.M{"id": entry["id"]},Main)
		connection.Update(bson.M{"id": entry["id"]},Description)

		connection.Update(bson.M{"id": entry["id"]},Temp)
		connection.Update(bson.M{"id": entry["id"]},TempMin)
		connection.Update(bson.M{"id": entry["id"]},TempMax)
		connection.Update(bson.M{"id": entry["id"]},Pressure)
		connection.Update(bson.M{"id": entry["id"]},SeaLevel)
		connection.Update(bson.M{"id": entry["id"]},GrndLevel)
		connection.Update(bson.M{"id": entry["id"]},Humidity)

		connection.Update(bson.M{"id": entry["id"]},Speed)
		connection.Update(bson.M{"id": entry["id"]},Deg)

		connection.Update(bson.M{"id": entry["id"]},Clouds)

	}

}
func GetDataFromAPIAndUpdateDB(entry map[string]interface{})  {
	url := ((entry["images"].(map[string]interface{}))["standard_resolution"]).(map[string]interface{})["url"].(string)
	client := clarifai.NewClient("-Hid-HDgn9zaWK9iTVrCfMsyMK7sBSJOxpCf2h63", "5yT6tenhHBAede35OEq5ImteiyIJCosMg342NTaA")
	// Get the current status of things
	info, err := client.Info()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("%+v\n", info)
	}
	// Let's get some context about these images
	urls:= []string{url}
	// Give it to Clarifai to run their magic
	tagData, err := client.Tag(clarifai.TagRequest{URLs: urls})
	fmt.Println("%+v\n", tagData.Results[0].Result.Tag)
	if err != nil {
		check(err)
	} else {
		UpdateTagsInDB(entry,tagData)
	}
	//get data from forecast, populates location coordinates
	Ln:=((entry["location"].(map[string]interface{}))["longitude"]).(float64)
	Lt:=((entry["location"].(map[string]interface{}))["latitude"]).(float64)

	w, err := owm.NewCurrent("C", "EN")
	w.CurrentByCoordinates(&owm.Coordinates{
		Longitude: Ln,
		Latitude: Lt,
	})
	fmt.Println(w)
	if err != nil {
		check(err)
	}else {
		UpdateWeatherDataInDB(entry, w)
	}

}
func InsertIntoDB(entry map[string]interface{}){

	connection:=PrepareConnection()
	count, err := connection.Find(bson.M{"id": entry["id"].(string)}).Limit(1).Count()
	if err != nil {
		check(err)
	}
	if count > 0 {
		fmt.Printf("resource %s already exists", entry["id"].(string))
		return
	}
	err = connection.Insert(entry)
	fmt.Println("%v",entry)


	GetDataFromAPIAndUpdateDB(entry)
}
// setup env variable OWM_API_KEY as "851b3239474c3c4c162da3e97656ac33"
func main(){
	f, err := os.Open("output.txt")
	check(err)
	scanner := bufio.NewScanner(f)
	var entry map[string]interface{}
	for scanner.Scan() {
		line:=scanner.Text()

		json.Unmarshal([]byte(line), &entry)
		InsertIntoDB(entry)

	}
}
